package bcas.inter.ap;

public interface A {
	String A = "AAA";

	String methodA();
}

interface B {
	String B = "BBB";

	String methodB();
}

class C implements A, B {
	public String methodA() {
		return A + B;

	}

	public String methodB() {
		return B + A;
	}
}

class D extends C implements A, B {
	String D = "DDD";

	public String methodA() {
		return D + methodB();
	}
}