package bcas.inter.ap;

public class MainClass {
	public static void main(String[] args) {
		C c = new C();

		System.out.println(c.methodA());
		System.out.println(c.methodB());

		c = new D();

		System.out.println(c.methodA());
		System.out.println(c.methodB());

	}
}
